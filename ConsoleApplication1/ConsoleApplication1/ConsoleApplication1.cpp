﻿#include <iostream>
#include <string>

void func(int N, bool key) {
	for (int i = 0; i <= N; i++) {
		if (key && i % 2 == 0 || !key && i % 2 == 1) {
			std::cout << std::to_string(i) << std::endl;
		}
		else {
			continue;
		}
	}
}

int main()
{
	int N = 10; //область действия переменной в теле программы, а не функции
	bool key = false; // true = even, false = odd
	//std::cin >> N;
	func(N, key);
}